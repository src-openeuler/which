Name:		which
Version:	2.21
Release:	17
Summary:	Show the full path of commands
License:	GPLv3
URL:		https://savannah.gnu.org/projects/which/
Source0:	http://ftp.gnu.org/gnu/which/%{name}-%{version}.tar.gz

Patch1:  0001-which-fails-for-long-path.patch
Patch2:  0002-coverity-fixes.patch

BuildRequires:	gcc

%description
Which takes one or more arguments. For each of its arguments it prints to stdout
the full path of the executables that would have been executed when this argument
had been entered at the shell prompt.

%package        help
Summary:        Help files for which
%description    help
Contains documents and manuals files for which

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure
%make_build

%install
%make_install

rm -f %{buildroot}/%{_datadir}/info/dir

%files
%license COPYING AUTHORS
%{_bindir}/%{name}

%files help
%doc EXAMPLES NEWS README
%{_mandir}/man1/%{name}.1.gz
%{_datadir}/info/%{name}.info.gz

%changelog
* Thu Nov 21 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 2.21-17
- fixed coverity issues

* Tue Apr 30 2024 Deyuan Fan <fandeyuan@kylinos.cn> - 2.21-16
- Type: bugfix
- ID: NA
- SUG: NA
- DESC: fix:which fails for long path

* Mon Jun 20 2022 yangzhao <yangzhao1@kylinos.cn> - 2.21-15
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary -S git

* Wed Jan 8 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.21-14
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: remove unnecessary files

* Thu Oct 10 2019 luhuaxin <luhuaxin@huawei.com> - 2.21-13
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move AUTHORS to license folder

* Wed Aug 28 2019 luhuaxin <luhuaxin@huawei.com> - 2.21-12
- Package init
